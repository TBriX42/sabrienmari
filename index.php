<?php

$hostUrl = 'http://'.$_SERVER['HTTP_HOST'];

if ($_SERVER['PHP_SELF'] != '/index.php')
  $selfUrl = $hostUrl.$_SERVER['PHP_SELF'];
else
  $selfUrl = $hostUrl.'/';

?>

<!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html lang="de" class="no-js ie6 oldie"> <![endif]-->
<!--[if IE 7]>    <html lang="de" class="no-js ie7 oldie"> <![endif]-->
<!--[if IE 8]>    <html lang="de" class="no-js ie8 oldie"> <![endif]-->
<!--[if IE 9]>    <html lang="de" class="no-js ie9"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 9]><!--> <html lang="de" class="no-js" itemscope="" itemtype="http://schema.org/Product"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <!--<meta name="robots" content="noindex">--> <!-- only for dev on subdomain remove on production mode -->

  <!-- Use the .htaccess and remove these lines to avoid edge case issues.
       More info: h5bp.com/b/378 -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>Sabrien Mari &bullet; Gesang &amp; Musik</title>

  <link rel="shortcut icon" href="/favicon.png" type="image/png">

  <meta name="author" content="Sabrien Mari">

  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">

  <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,200,200italic,300italic,600,600italic' rel='stylesheet' type='text/css'>

  <meta name="description" itemprop="description" content="Sabrien Mari, passionierte Sängerin und Musikerin. Auftritte in ganz Deutschland. Anfrage und Buchung per Mail.">

  <meta name="keywords" content="Sabrien Mari, Sabrien, Mari, Sängerin, Gesang, Musikerin, Musik, Auftritt, Auftritte, Deutschland, Buchung">

  <link rel="canonical" href="<?php echo $selfUrl; ?>" />
  <meta property="og:title" content="Sabrien Mari • Gesang &amp; Musik" />
  <meta property="og:type" content="article" />
  <meta property="og:url" content="<?php echo $selfUrl; ?>" />
  <meta property="og:image" content="<?php echo $hostUrl; ?>/assets/img/elements/og_image.jpg" />
  <meta property="og:image:type" content="image/jpeg" />
  <meta property="og:site_name" content="Sabrien Mari • Gesang &amp; Musik" />
  <meta property="og:description" content="Sabrien Mari, passionierte Sängerin und Musikerin. Auftritte in ganz Deutschland. Anfrage und Buchung per Mail." />
  <meta property="article:publisher" content="https://www.facebook.com/SabrienMari" />
  <meta property="article:published_time" content="2015-05-20T12:02:48Z" />
  <meta property="article:modified_time" content="2015-05-20T12:02:48Z" />

  <link rel="stylesheet" href="assets/css/normalize.css">
  <link rel="stylesheet" href="assets/css/main.css">
</head>

<body>
  <div id="site-wrapper">
    <div id="bg"></div>
    <div id="main" class="container">
      <header id="header" class="">
        <h1 class="hidden">Sabrien Mari</h1>
        <img src="/assets/img/elements/logo.svg" alt="Sabrien Mari" id="logo">
      </header><!-- /header -->
      <section role="main">
        <h2></h2>
        <div id="slogan">
          <span class="line line-1">Into the nature,</span><br/>
          <span class="line line-2">Into the wild,</span><br/>
          <span class="line line-3">With music</span><br/>
          <span class="line line-4">Always by my side</span>
        </div>

        <div class="sounds">
          <audio data-ref-line-class="line-1">
            <source src="/sounds/Adele_(cover-_Sabrien_M.)_-_Make_you_feel_my_love.mp3"/>
            <source src="/sounds/Adele_(cover-_Sabrien_M.)_-_Make_you_feel_my_love.ogg"/>
            Your browser doesn't support html5 audio.
          </audio>
          <audio data-ref-line-class="line-2">
            <source src="/sounds/Madsen_(cover-_Sabrien_M.)_-_So_cool_bist_du_nicht.mp3"/>
            <source src="/sounds/Madsen_(cover-_Sabrien_M.)_-_So_cool_bist_du_nicht.ogg"/>
            Your browser doesn't support html5 audio.
          </audio>
          <audio data-ref-line-class="line-3">
            <source src="/sounds/Jenifer_Brening_(cover-_Sabrien_Mari)_-_Not_that_Guy.mp3"/>
            <source src="/sounds/Jenifer_Brening_(cover-_Sabrien_Mari)_-_Not_that_Guy.ogg"/>
            Your browser doesn't support html5 audio.
          </audio>
        </div>
      </section>

      <a href="" class="imprint-toggle">Imprint</a>
      <aside id="imprint">
        <h2>Impressum</h2>
        <section>
          <h2>Angaben gem&auml;&szlig; &sect; 5 Telemediengesetz (TMG):</h2>
          Sabrien Mari<br/>
          Boeselagerstr 71b, App. 019<br/>
          48163 M&uuml;nster<br/>
          Telefon: +49 162 6167472<br/>
          E-Mail: <a href="mailto:contact@sabrienmari.com">contact@sabrienmari.com</a>
        </section>

        <section>
          <h2>Inhaltlich Verantwortlicher gem. &sect; 55 II RStV:</h2>
          Sabrien Mari (Anschrift s. o.)
        </section>

        <section>
          <h2>Gestaltung &amp; Umsetzung</h2>
          Thorben Ziegler<br/>
          E-Mail: <a href="mailto:contact@thorbenziegler.de">contact@thorbenziegler.de</a><br/>
          <a target="_blank" href="http://www.thorbenziegler.de">www.thorbenziegler.de</a>
        </section>
      </aside>

      <footer>
        <a href="http://www.youtube.de/user/CrazyBrinix3" id="youtube"><span class="icon"></span>/CrazyBrinix3</a>
        <a href="http://www.facebook.de/SabrienMari" id="facebook"><span class="icon"></span>/SabrienMari</a>
      </footer>
    </div>
    <a target="_blank" id="thorben-ziegler" href="http://www.thorbenziegler.de">www.thorbenziegler.de</a>
  </div>

  <!-- Grab Google CDN's jQuery, fall back to local if offline -->
  <!-- 2.0 for modern browsers, 1.10 for .oldie -->
  <script>
  var oldieCheck = Boolean(document.getElementsByTagName('html')[0].className.match(/\soldie\s/g));
  if(!oldieCheck) {
  document.write('<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"><\/script>');
  } else {
  document.write('<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"><\/script>');
  }
  </script>
  <script>
  if(!window.jQuery) {
  if(!oldieCheck) {
    document.write('<script src="assets/js/jquery-2.0.2.min.js"><\/script>');
  } else {
    document.write('<script src="assets/js/libs/jquery-1.10.1.min.js"><\/script>');
  }
  }
  </script>

  <script type="text/javascript" src="assets/js/main.js"></script>
  <script type="text/javascript" src="assets/js/sound.js"></script>

  </body>
</html>