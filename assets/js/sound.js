var analyser = new Array();
var canvas = null;
var ctx = null;
var source = new Array();
var fbc_array = null;
var index;

var bars, bar_x, bar_width, bar_height;

//handle different prefix of the audio context
var AudioContext = AudioContext || webkitAudioContext;
//create the context.
var context = new AudioContext();

//using requestAnimationFrame instead of timeout...
if (!window.requestAnimationFrame)
  window.requestAnimationFrame = window.webkitRequestAnimationFrame;

jQuery(function () {
  jQuery('.line').hover(function() {
    if (jQuery(window).width() > 650) {
      var $audio = jQuery('[data-ref-line-class="' + jQuery(this).attr("class").match(/line-.*/)  + '"]');
      $audio.prop('volume', 0);
      $audio.get()[0].play();
      $audio.animate({volume: 1}, 1000);
      init(jQuery(this), $audio);
    }
  }, function() {
    if (jQuery(window).width() > 650) {
      var $audio = jQuery('[data-ref-line-class="' + jQuery(this).attr("class").match(/line-.*/)  + '"]');
      $audio.animate({volume: 0}, 1000, function() {
      $audio.get()[0].pause();
      });
    }
  });
});

function init($element, $audio) {
  //add new canvas
  if ($element.find('canvas').length == 0)
    $($element).prepend('<canvas width="60px" height="'+ $element.height() +'"></canvas>');

  index = $element.attr("class").match(/line-.*/);

  canvas = $element.find('canvas').get()[0];
  //  jQuery(canvas).fadeIn(500);

  //get context from canvas for drawing
  ctx = canvas.getContext("2d");

  if (typeof source[index] == 'undefined') {
    analyser[index] = context.createAnalyser();

    source[index] = context.createMediaElementSource($audio.get()[0]);
    source[index].connect(analyser[index]);
    analyser[index].connect(context.destination);
  }

  frameLooper();
}

function frameLooper(){
  window.requestAnimationFrame(frameLooper);
  fbc_array = new Uint8Array(analyser[index].frequencyBinCount);
  analyser[index].getByteFrequencyData(fbc_array);
  ctx.clearRect(0, 0, canvas.width, canvas.height); // Clear the canvas
  ctx.fillStyle = '#000000'; // Color of the bars
  bars = 5;
  for (var i = 0; i < bars; i++) {
    bar_width = 3;
    bar_x = (jQuery(canvas).width() / bars) * i;
    bar_height = -(fbc_array[i] / 4);
    //fillRect( x, y, width, height ) // Explanation of the parameters below
    ctx.fillRect(bar_x, canvas.height, bar_width, bar_height);
  }
}