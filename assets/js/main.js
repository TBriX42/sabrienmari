jQuery(function () {
  jQuery('html').removeClass('no-js').addClass('js');

  jQuery('#slogan .line').css('opacity', 0);
  jQuery('body').hide()

  if (jQuery(window).width() >= 1000)
    jQuery('#logo').hide();

  // toggle imprint
  jQuery('.imprint-toggle').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    jQuery('#imprint').toggleClass('active');
  });

  // hide imprint on click outside
  jQuery('html').click(function() {
    jQuery('#imprint').removeClass('active');
  });
});


jQuery(window).load(function() {
  jQuery('body').fadeIn(1000, function() {
    jQuery('#slogan .line').each(function(i) {
      jQuery(this).delay(1000*i).css('opacity',0).animate({'opacity': 1}, 1000);
    });

    if (jQuery(window).width() >= 1000)
      jQuery('#logo').delay(jQuery('#slogan .line').length*1000).fadeIn(1000);
  });
});